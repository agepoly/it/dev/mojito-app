import { defineConfig } from "windicss/helpers";

export default defineConfig({
  content: ["./index.html", "./src/**/*.{vue,js,ts,jsx,tsx}"],
  theme: {
    extend: {
      colors: {
        "theme-background": "var(--theme-background)",
        "theme-background-accent": "var(--theme-background-accent)",
        "theme-background-darker": "var(--theme-background-darker)",
        "theme-accent": "var(--theme-accent)",
        "theme-accent-darker": "var(--theme-accent-darker)",
        "theme-text": "var(--theme-text)",
        "theme-text-darker": "var(--theme-text-darker)",
        "theme-text-link-hover": "var(--theme-text-link-hover)",
      },
      screens: {
        "small-navbar": "768px",
      },
      fontSize: {
        "2xs": "0.50rem",
        "3xs": "0.25rem",
      },
    },
  },
  shortcuts: {},
  darkMode: "class",
  plugins: [],
  extract: {
    include: ["./**/*.vue", "node_modules/tailvue/dist/tailvue.es.js"],
  },
});
