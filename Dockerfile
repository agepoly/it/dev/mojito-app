FROM node:21.7.3-alpine AS node_build
WORKDIR /node
COPY package*.json ./
RUN npm i
COPY . ./
RUN npm run build-only

FROM nginx:alpine3.19-otel
WORKDIR /var/www
COPY ./nginx/nginx.conf /etc/nginx/nginx.conf
COPY --from=node_build /node/dist .

