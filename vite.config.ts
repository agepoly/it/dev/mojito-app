import { fileURLToPath, URL } from "node:url";

import { defineConfig } from "vite";
import vue from "@vitejs/plugin-vue";
import VitePluginWindicss from "vite-plugin-windicss";

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [vue(), VitePluginWindicss()],
  resolve: {
    alias: {
      "@": fileURLToPath(new URL("./src", import.meta.url)),
      api: fileURLToPath(new URL("./whiskey-openapi", import.meta.url)),
    },
  },
  server: {
    host: "0.0.0.0",
    port: 5000,
    proxy: {
      "^/api": {
        ws: false,
        changeOrigin: true,
        target: "http://127.0.0.1:4000",
        secure: false,
      },
    },
  },
});
