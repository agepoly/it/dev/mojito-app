# Whiskey !
> AGEPoly's new SSO, account management and authorization system

## Project Setup

```sh
npm install
```

### Compile and Hot-Reload for Development

```sh
npm run dev
```

### Type-Check, Compile and Minify for Production

```sh
npm run build
```

### Lint with [ESLint](https://eslint.org/)

```sh
npm run lint
```

### Open API Generator
1. Install the CLI
```sh
npm i -g @openapitools/openapi-generator-cli
```
2. Get the current openapi.json file from the running backend server and generate src/whiskey-openapi
```sh
npm run openapi # equivalent to `npm run openapi-get && npm run openapi-gen`
```