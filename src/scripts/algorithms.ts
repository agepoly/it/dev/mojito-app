import type { Unit } from "@/scripts/api-helper";
import type { UnitsTree } from "@/scripts/client-types";

export function unitFromCode(unit_code: string, units: Unit[]): Unit | null {
  for (const unit of units) {
    if (unit.code === unit_code) return unit;
  }
  return null;
}

/**
 * Builds an array of all sub_units from the given unit_code and the array of all units
 */
export function buildSubUnitTrees(code: string, units: Unit[]): UnitsTree[] {
  const sub_units = <UnitsTree[]>[];
  for (const u of units) {
    if (u.parent_code === code) {
      sub_units.push(unitToUnitsTree(u, units));
    }
  }
  return sub_units.sort((a, b) => a.code.localeCompare(b.code));
}

/**
 * Takes a unit and returns its UnitsTree form.
 * Its sub_units array is built recursively with the array of all units
 * by calling the buildSubUnitTree(..) function
 */
export function unitToUnitsTree(unit: Unit, units: Unit[]): UnitsTree {
  return {
    code: unit.code,
    display_name: unit.display_name,
    epfl_id: unit.epfl_id,
    sub_units: buildSubUnitTrees(unit.code, units),
  };
}

/**
 * Builds an array of root unit (as UnitsTree[]) from the list of all units (Unit[])
 */
export function buildUnitsTree(units: Unit[]): UnitsTree[] {
  const roots = <UnitsTree[]>[];
  for (const unit of units) {
    if (!unit.parent_code) {
      roots.push(unitToUnitsTree(unit, units));
    }
  }
  return roots.sort((a, b) => a.code.localeCompare(b.code));
}

/**
 * Compute the path of the given unit
 * @returns the path as a string array in this order : ['top-level parent', ..., unit.code]
 */
export function findUnitPath(unit: Unit, units: Unit[]): string[] {
  if (!unit.parent_code) return [unit.code];
  return [
    ...findUnitPath(unitFromCode(unit.parent_code, units)!, units),
    unit.code,
  ];
}
