import { Fetcher, type Middleware } from "openapi-typescript-fetch";
import type { paths, components } from "@/whiskey-api";
import { useSessionStore } from "@/stores/session";

export type Unit = components["schemas"]["UnitDb"];
export type Group = components["schemas"]["GroupDb"];
export type UserSessionRaw = components["schemas"]["UserSessionWithJWT"];

const authMiddleware: Middleware = async (url, init, next) => {
  const jwt = useSessionStore().jwt;
  if (jwt) {
    init.headers.set("Authorization", `Bearer ${jwt}`);
  }
  return await next(url, init);
};

const urlOverride = localStorage.getItem("backend_url");
if (urlOverride) {
  console.log(`Backend URL override : ${urlOverride}/api`);
}
const fetcher = Fetcher.for<paths>();
fetcher.configure({
  baseUrl: urlOverride
    ? `${urlOverride}/api`
    : "https://whiskey-api.agepoly.ch/api",
  use: [authMiddleware],
});

// /api/hydra

export const hydraGetLoginRequest = fetcher
  .path("/hydra/login-request")
  .method("get")
  .create();
export const hydraPostLoginRequestLocal = fetcher
  .path("/hydra/login-request/local")
  .method("post")
  .create();
export const hydraPostLoginRequestTequila = fetcher
  .path("/hydra/login-request/tequila")
  .method("post")
  .create();

export const hydraGetConsentRequest = fetcher
  .path("/hydra/consent-request")
  .method("get")
  .create();
export const hydraPostConsentRequest = fetcher
  .path("/hydra/consent-request")
  .method("post")
  .create();

// /api/login

export const loginPostLogin = fetcher.path("/login").method("post").create();
export const loginPostTequilaRequest = fetcher
  .path("/login/tequila-request")
  .method("post")
  .create();
export const loginPostTequilaCallback = fetcher
  .path("/login/tequila-callback")
  .method("post")
  .create();

// /api/units

export const unitsGetUnits = fetcher.path("/units").method("get").create();

export const groupsGetGroups = fetcher.path("/groups").method("get").create();
export const groupsGetGroup = fetcher
  .path("/groups/{uid}")
  .method("get")
  .create();

export const usersGetUsers = fetcher.path("/users").method("get").create();
export const usersGetUser = fetcher.path("/users/{uid}").method("get").create();
export const usersGetGroups = fetcher
  .path("/users/{user}/groups")
  .method("get")
  .create();
export const usersPostUserCredentials = fetcher
  .path("/users/{uid}/credentials")
  .method("post")
  .create();