import { ref } from "vue";

const screens = {
  sm: 640,
  md: 768,
  lg: 1024,
  xl: 1280,
};

function effectiveBreakpoint(w: number) {
  if (w >= screens.xl) return "xl";
  else if (w >= screens.lg) return "lg";
  else if (w >= screens.md) return "md";
  else return "sm";
}

function getBreakpoints(w: number) {
  return {
    width: w,
    effective: effectiveBreakpoint(w),
    sm: w >= screens.sm,
    md: w >= screens.md,
    lg: w >= screens.lg,
    xl: w >= screens.xl,
  };
}

const breakpoints = ref(getBreakpoints(window.innerWidth));

window.addEventListener("resize", () => {
  breakpoints.value = getBreakpoints(window.innerWidth);
});

export default breakpoints;
