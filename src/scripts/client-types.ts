export type BreadcrumbRoute = {
  text: string;
  path: string;
};

export type UnitsTree = {
  code: string;
  display_name: string;
  epfl_id?: number | null;
  sub_units: UnitsTree[];
};