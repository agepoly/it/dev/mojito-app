import { defineStore } from "pinia";
import { unitsGetUnits } from "@/scripts/api-helper";
import type { Unit } from "@/scripts/api-helper";
import type { UnitsTree } from "@/scripts/client-types";
import { buildUnitsTree } from "@/scripts/algorithms";

export type DataStore = {
  unitsArray: Unit[] | null;
  unitsTree: UnitsTree[] | null;
};

export const useDataStore = defineStore({
  id: "data",

  state: (): DataStore => {
    return {
      unitsArray: null,
      unitsTree: null,
    };
  },

  actions: {
    async fetchUnits(refresh = false): Promise<Unit[] | null> {
      if (!this.unitsArray || refresh) {
        try {
          this.unitsArray = (await unitsGetUnits({})).data;
          this.unitsTree = buildUnitsTree(this.unitsArray);
          return this.unitsArray;
        } catch (err) {
          // TODO display with notification store
          console.error("Failed to fetch units : ", err);
          return null;
        }
      } else {
        return this.unitsArray;
      }
    },
  },
});
