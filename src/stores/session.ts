import { defineStore } from "pinia";

import type { Group, Unit, UserSessionRaw } from "@/scripts/api-helper";

export type SessionStore = {
  sciper: number | undefined | null;
  display_name: string;
  uid: string | undefined | null;
  username: string | undefined | null;
  email: string;
  units: Unit[];
  groups: Group[];
  // Session info
  session_uid: string | null;
  expired_at: Date | null;
  jwt: string | null;
};

function applySessionTo(sessionRaw: UserSessionRaw, to: SessionStore) {
  to.sciper = sessionRaw.user_session.sciper;
  to.display_name = sessionRaw.user_session.display_name;
  to.uid = sessionRaw.user_session.uid;
  to.username = sessionRaw.user_session.username;
  to.email = sessionRaw.user_session.email;
  to.units = sessionRaw.user_session.units;
  to.groups = sessionRaw.user_session.groups;
  to.session_uid = sessionRaw.user_session.session_uid;
  to.expired_at = new Date(
    sessionRaw.user_session.expired_at.split(".")[0] + "Z"
  );
  to.jwt = sessionRaw.jwt;
}

export const useSessionStore = defineStore({
  id: "session",

  state: (): SessionStore => {
    const sessionStr = localStorage.getItem("session");
    const session: SessionStore = sessionStr ? JSON.parse(sessionStr) : null;
    if (session) {
      console.log("session recovered from local storage :", session);
      return session;
    }
    return <SessionStore>{};
  },

  actions: {
    isActive() {
      return this.uid != null; // todo handle state.expired_at
    },
    isAdmin() {
      return !!this.groups.find((g) => g.abv === "ACCRED" && !g.unit_code);
    },
    setSession(sessionRaw: UserSessionRaw) {
      console.log("Setting session");
      applySessionTo(sessionRaw, this);
      const session = <SessionStore>{};
      applySessionTo(sessionRaw, session);
      localStorage.setItem("session", JSON.stringify(session));
      console.log("Session saved and set to: ", session);
    },
    clearSession() {
      localStorage.removeItem("session");
      this.uid = null;
      console.log("Session cleared");
    },
  },
});
