import { createRouter, createWebHistory } from "vue-router";
import type { RouteLocationNormalized } from "vue-router";
import { useSessionStore } from "@/stores/session";
import LoginView from "@/views/login/LoginView.vue";
import ConsentView from "@/views/login/ConsentView.vue";
import Dashboard from "@/views/main/Dashboard.vue";
import Groups from "@/views/main/Groups.vue";
import GroupDetail from "@/views/main/GroupDetail.vue";
import Units from "@/views/main/Units.vue";
import UnitDetails from "@/views/main/UnitDetails.vue";
import UserDetails from "@/views/main/UserDetails.vue";
import Users from "@/views/main/Users.vue";
import MyAccount from "@/views/main/MyAccount.vue";

function isSessionActive(
  to: RouteLocationNormalized,
  from: RouteLocationNormalized
) {
  const active = useSessionStore().isActive();
  if (to.name === "login" || to.name === "home") {
    return active ? { name: "dashboard" } : true;
  } else {
    return active ? true : { name: "login" };
  }
}

function isAdminSession(
  to: RouteLocationNormalized,
  from: RouteLocationNormalized
) {
  const active = useSessionStore().isAdmin();
  return active ? true : { name: "forbidden" };
}

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: "/",
      name: "home",
      redirect: {
        name: "login",
      },
      beforeEnter: isSessionActive,
    },
    {
      path: "/login",
      name: "login",
      component: LoginView,
      beforeEnter: isSessionActive,
    },
    {
      path: "/logout",
      name: "logout",
      component: () => import("@/views/login/LogoutView.vue"),
      beforeEnter: () => {
        useSessionStore().clearSession();
      },
    },
    {
      path: "/consent",
      name: "consent",
      component: ConsentView,
    },
    {
      path: "/dashboard",
      name: "dashboard",
      component: Dashboard,
      beforeEnter: isSessionActive,
    },
    {
      path: "/groups",
      name: "groups",
      component: Groups,
      beforeEnter: isSessionActive,
      children: [
        {
          path: ":group_uid",
          name: "group_details",
          component: GroupDetail,
          beforeEnter: isSessionActive,
        },
      ],
    },
    {
      path: "/units",
      name: "units",
      component: Units,
      beforeEnter: isSessionActive,
      children: [
        {
          path: ":unit_code",
          name: "unit_details",
          component: UnitDetails,
          beforeEnter: isSessionActive,
        },
      ],
    },
    {
      path: "/users",
      name: "users",
      component: Users,
      beforeEnter: isSessionActive,
      children: [
        {
          path: ":uid",
          name: "user_details",
          component: UserDetails,
          beforeEnter: isSessionActive,
        },
      ],
    },
    {
      path: "/my-account",
      name: "my-account",
      component: MyAccount,
      beforeEnter: isSessionActive,
    },

    {
      path: "/local-settings",
      name: "local-settings",
      component: () => import("@/views/public/LocalSettingsOverride.vue"),
    },

    {
      path: "/admin",
      name: "admin",
      component: () => import("@/views/admin/AdminHome.vue"),
      beforeEnter: isAdminSession,
      children: [
        {
          path: "unit-creation",
          name: "unit-creation",
          component: () => import("@/views/admin/AdminUnitCreation.vue"),
          beforeEnter: isAdminSession,
        },
        {
          path: "user-creation",
          name: "user-creation",
          component: () => import("@/views/admin/AdminUserCreation.vue"),
          beforeEnter: isAdminSession,
        },
        {
          path: "user-edition/:uid",
          name: "user-edition-uid",
          component: () => import("@/views/admin/AdminUserEdition.vue"),
          beforeEnter: isAdminSession,
        },
      ],
    },

    {
      path: "/forbidden",
      name: "forbidden",
      component: () => import("@/views/public/Forbidden.vue"),
    },

    {
      path: "/:pathMatch(.*)*",
      name: "not-found",
      component: import("@/views/public/NotFoundView.vue"),
    },
  ],
});

export default router;
