import { createApp } from "vue";
import { createPinia } from "pinia";
import { createVuetify } from "vuetify";
import * as components from "vuetify/components";
import * as directives from "vuetify/directives";
import { VueQueryPlugin } from "vue-query";

const app = createApp(App);
app.use(createPinia());

import App from "./App.vue";
import router from "./router";

app.use(router);

app.use(VueQueryPlugin);

import "./assets/main.css";
import "virtual:windi.css";
import "@mdi/font/css/materialdesignicons.css";
import "vuetify/styles";

import Link from "@/components/ui/WLink.vue";

app.component("app-link", Link);

export const vuetify = createVuetify({
  theme: {
    themes: {
      light: {
        colors: {
          primary: "#1867C0",
          secondary: "#374151",
        },
      },
      dark: {
        colors: {
          primary: "#aa67C0",
          secondary: "#aa4151",
        },
      },
    },
  },
  components,
  directives,
  locale: {
    // adapter: createVueI18nAdapter({ i18n, useI18n })
  },
});

app.use(vuetify);

import Vue3EasyDataTable from "vue3-easy-data-table";
import "vue3-easy-data-table/dist/style.css";

app.component("EasyDataTable", Vue3EasyDataTable);

import("webfontloader").then((webFontLoader) => {
  webFontLoader.load({
    google: {
      families: ["Roboto:100,300,400,500,700,900&display=swap"],
    },
  });
});

app.mount("#app");
